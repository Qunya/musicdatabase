package app.servlets;

import app.entities.Artist;
import app.model.Model;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;

public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/add.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        String firstName = req.getParameter("firstName");
        String surName = req.getParameter("surName");
        String secondName = req.getParameter("secondName");
        String dateOfBirth = req.getParameter("dateOfBirthday");
        Date dateOfBirthday = Date.valueOf(dateOfBirth);
        String dateOfStartCareer = req.getParameter("dateOfStartCareer");
        Date dateOfSC = Date.valueOf(dateOfStartCareer);
        String dateOfEndCareer = req.getParameter("dateOfEndCareer");
        Date dateOfEC = Date.valueOf(dateOfEndCareer);
        String sex = req.getParameter("sex");
        String country = req.getParameter("country");
        String city = req.getParameter("city");
        String genres = req.getParameter("genres");
        String description = req.getParameter("description");
        Artist artist = new Artist(firstName, secondName, surName, dateOfBirthday, dateOfSC, dateOfEC, sex, country, city, genres, description);
        Model model = Model.getInstance();
        model.add(artist);
    }
}
