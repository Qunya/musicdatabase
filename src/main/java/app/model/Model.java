package app.model;

import app.entities.Artist;
import app.entities.Label;
import app.entities.MusicalGroup;
import app.logics.DBManager;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Model {
    Collection<Label> labelList;
    Collection<MusicalGroup> groupList;
    Collection<Artist> artistList;
    DBManager dbManager = DBManager.getInstance();

    private static Model instance = new Model();

    public static synchronized Model getInstance() {
        return instance;
    }

    private Model() {
        refresher();
    }

    //Добавление объекта в базу
    public void add(Artist artist) {
        dbManager.insertObjectIntoDatabase(artist);
    }

    public void refresher(){
        labelList = dbManager.loadLabelListFromDatabase();
        groupList = dbManager.loadMusicalGroupsFromDatabase();
        artistList = dbManager.loadPerfomersFromDatabase();
    }

    //Вывод лейблов
    public List<String> outputLabel() {
        return labelList.stream().map((s)->s+"").collect(Collectors.toList());
    }

    //Вывод музыкальной группы
    public List<String> outputMusicalGroup() {
        return groupList.stream().map((s)->s+"").collect(Collectors.toList());
    }

    //Вывод артисты
    public List<String> outputArtist() {
        return artistList.stream().map((s)->s+"").collect(Collectors.toList());
    }
}
