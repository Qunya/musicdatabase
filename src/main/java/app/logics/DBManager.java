package app.logics;

import app.entities.Artist;
import app.entities.Label;
import app.entities.MusicalGroup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.TreeSet;
import java.util.logging.Logger;

public class DBManager {
    private static final String GetPefomersFromDB = "SELECT * FROM perfomers ORDER BY perfomer_id";
    private static final String GetMusicalGroupFromDB = "SELECT * FROM musicalGroups ORDER BY musicalGroup_id";
    private static final String GetLabelFromDB = "SELECT * FROM labels ORDER BY label_id";
    private static final String InsertPerfomersIntoDB = "INSERT INTO perfomers (firstName, secondName, surName,\n" +
            "dateOfBirthday, dateOfStartCareer, dateOfEndCareer, sex, country, city, genres,\n" +
            "description, label_id, musicalGroup_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String InsertMusicalGroupIntoDB = "INSERT INTO labels (nameOfLabel, description, founder,\n" +
            "founded, location, countryOfOrigin) VALUES (?, ?, ?, ?, ?, ?))";
    private static final String InsertLabelIntoDB = "INSERT INTO musicalgroups (nameOfMusicalGroup, description," +
            "label_id, dateOfStartCareer, dateOfEndCareer, genres, country, city) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?))";
    private static final String GetPerfomersFromLabel = "SELECT * FROM perfomers WHERE label_id=?";
    private static final String GetPerfomersFromMusicGroup = "SELECT * FROM perfomers WHERE musicalGroup_id=?";
    private static final String GetMusicalGroupsFromLabel = "SELECT * FROM musicalgroups WHERE label_id=?";
    private static final String MovePerfomersIntoOtherMusicGroup = "UPDATE perfomers SET musicalGroup_id=? WHERE perfomer_id=?," +
            "musicalGroup_id=?";
    private static final String MovePerfomersIntoOtherLabel = "UPDATE perfomers SET label_id=? WHERE perfomer_id=?," +
            "label_id=?";
    private static final String MoveMusicalGroupsIntoOtherLabel = "UPDATE musicalgroups SET label_id=? WHERE musicalGroup_id=?," +
            "label_id=?";
    private static final String UpdatePerfomerData = "UPDATE perfomers SET firstName=?, secondName=?, surName=?," +
            "dateOfBirthday=?, dateOfStartCareer=?, dateOfEndCareer=?, sex=?, country=?, city=?, genres=?, " +
            "description=?, label_id=?, musicalGroup_id=? WHERE perfomer_id=?";
    private static final String UpdateMusicalGroupData = "UPDATE musicalGroups SET nameOfMusicalGroup=?, description=?," +
            "label_id=?, dateOfStartCareer=?, dateOfEndCareer=?, genres=?, country=?, city=? " +
            "WHERE musicalGroup_id=?";
    private static final String UpdateLabelData = "UPDATE labels SET nameOfLabel=?, description=?, founder=?," +
            "founded=?, location=?, countryOfOrigin=? WHERE label_id=?";
    private static final String DeletePerfomer = "DELETE FROM perfomers WHERE perfomer_id=?";
    private static final String DeleteMusicalGroup = "DELETE FROM musicalGroups WHERE musicalGroup_id=?";
    private static final String DeleteLabel = "DELETE FROM labels WHERE label_id=?";

    private static DBManager ourInstance = new DBManager();
    private static Logger logger = Logger.getLogger(DBManager.class.getName());

    private DBManager() {
    }

    private Connection connectionInit() {
        Connection tmpConnection;
        Properties properties = propertiesInit();
        try {
            Class.forName(properties.getProperty("Database.DriverUrl"));
            tmpConnection = DriverManager.getConnection(properties.getProperty("Database.ConnectionUrl"), properties.getProperty("Database.UserLogin"), properties.getProperty("Database.Password"));
            return tmpConnection;
        } catch (ClassNotFoundException cnfex) {
            logger.info("ClassNotFoundException in connectionInit" + cnfex.getMessage());
        } catch (SQLException sqlex) {
            logger.info("SQLException in connectionInit" + sqlex.getMessage());
        }
        return null;
    }

    private Properties propertiesInit() {
        Properties tmpPropertirs = new Properties();
        try {
            tmpPropertirs.load(new FileInputStream(new File("C:\\Users\\ReaGame\\IdeaProjects\\MusicalSocialFloor\\src\\main\\resources\\config.properties")));
        } catch (FileNotFoundException fnfex) {
            logger.info("FileNotFoundException in propertiesInit \n" + fnfex.getMessage());
        } catch (IOException ioex) {
            logger.info("IOException in propertiesInit \n" + ioex.getMessage());
        }
        return tmpPropertirs;
    }

    public static synchronized DBManager getInstance() {
        return ourInstance;
    }

    //Выгружаем весь список музыкантов из базы данных
    public Collection<Artist> loadPerfomersFromDatabase() {
        Collection<Artist> perfomers = new TreeSet<Artist>();
        Connection connection = connectionInit();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GetPefomersFromDB);
            while (resultSet.next()) {
                perfomers.add(new Artist(resultSet));
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in loadPerfomersFromDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveResultSet(resultSet);
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
        return perfomers;
    }

    //Выгружаем весь список музыкальных групп из базы данных
    public Collection<MusicalGroup> loadMusicalGroupsFromDatabase() {
        Collection<MusicalGroup> musicalGroups = new TreeSet<MusicalGroup>();
        Connection connection = connectionInit();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GetMusicalGroupFromDB);
            while (resultSet.next()) {
                musicalGroups.add(new MusicalGroup(resultSet));
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in loadMusicalGroupsFromDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveResultSet(resultSet);
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
        return musicalGroups;
    }

    //Выгружаем весь список лейблов из базы данных
    public Collection<Label> loadLabelListFromDatabase() {
        Collection<Label> labelList = new ArrayList<Label>();
        Connection connection = connectionInit();
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(GetLabelFromDB);
            while (resultSet.next()) {
                labelList.add(new Label(resultSet));
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in loadLabelListFromDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveResultSet(resultSet);
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
        return labelList;
    }

    //Добавляем нового объекта в базу
    public void insertObjectIntoDatabase(Artist artist) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(InsertPerfomersIntoDB);
            statement.setString(1, artist.getFirstName());
            statement.setString(2, artist.getSecondName());
            statement.setString(3, artist.getSurName());
            statement.setDate(4, artist.getDateOfBirthday());
            statement.setDate(5, artist.getDateOfStartCareer());
            statement.setDate(6, artist.getDateOfEndCareer());
            statement.setString(7, String.valueOf(artist.getSex()));
            statement.setString(8, artist.getCountry());
            statement.setString(9, artist.getCity());
            statement.setString(10, artist.getGenres());
            statement.setString(11, artist.getDescription());
            statement.setInt(12, artist.getLabelId());
            statement.setInt(13, artist.getMusicalGroupId());
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in insertObjectIntoDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    public void insertObjectIntoDatabase(Label label) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(InsertMusicalGroupIntoDB);
            statement.setString(1, label.getNameOfLabel());
            statement.setString(2, label.getDescription());
            statement.setString(3, label.getFounder());
            statement.setDate(4, label.getFounded());
            statement.setString(5, label.getLocation());
            statement.setString(6, label.getCountryOfOrigin());
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in insertObjectIntoDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    public void insertObjectIntoDatabase(MusicalGroup musicalGroup) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(InsertLabelIntoDB);
            statement.setString(1, musicalGroup.getNameOfMusicalGroup());
            statement.setString(2, musicalGroup.getDescription());
            statement.setInt(3, musicalGroup.getLabelId());
            statement.setDate(4, musicalGroup.getDateOfStartCareer());
            statement.setDate(5, musicalGroup.getDateOfEndCareer());
            statement.setString(6, musicalGroup.getGenres());
            statement.setString(7, musicalGroup.getCountry());
            statement.setString(8, musicalGroup.getCity());
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in insertObjectIntoDatabase" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Получаем список музыкантов из какого-то определённого лейбла
    public Collection<Artist> getPerfomersFrom(int objectId, boolean flag) {
        Collection<Artist> tmpArtists = new TreeSet<Artist>();
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            if (flag) {
                statement = connection.prepareStatement(GetPerfomersFromLabel);
            } else {
                statement = connection.prepareStatement(GetPerfomersFromMusicGroup);
            }
            statement.setInt(1, objectId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                tmpArtists.add(new Artist(resultSet));
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in getPerfomersFrom" + sqlex.getMessage());
        } finally {
            doCloseActiveResultSet(resultSet);
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
        return tmpArtists;
    }

    //Получаем список музыкантов из какого-то определённого лейбла
    public Collection<MusicalGroup> getMusicalGroupsFromLabel(int labelId) {
        Collection<MusicalGroup> tmpMusicalGroups = new TreeSet<MusicalGroup>();
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(GetMusicalGroupsFromLabel);
            statement.setInt(1, labelId);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                tmpMusicalGroups.add(new MusicalGroup(resultSet));
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in getMusicalGroupsFromLabel" + sqlex.getMessage());
        } finally {
            doCloseActiveResultSet(resultSet);
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
        return tmpMusicalGroups;
    }

    //Переводим артиста из одной группы в другую
    //Переводим артиста из одного лейбла в другой
    //Переводим музыкальную группу из одного лейбла в другой
    public void moveObjectTo(int objectId, int oldMovementId, int newMovementId, int flag) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        String moveObjectInto = "";
        try {
            switch (flag) {
                case 1:
                    moveObjectInto = MovePerfomersIntoOtherLabel;
                    break;
                case 0:
                    moveObjectInto = MoveMusicalGroupsIntoOtherLabel;
                    break;
                case -1:
                    moveObjectInto = MovePerfomersIntoOtherMusicGroup;
                    break;
            }
            statement = connection.prepareStatement(moveObjectInto);
            statement.setInt(1, newMovementId);
            statement.setInt(2, objectId);
            statement.setInt(3, oldMovementId);
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in moveObjectTo" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Обновляем данные об артисте/музыканте
    public void updateData(Artist artist) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UpdatePerfomerData);
            statement.setString(1, artist.getFirstName());
            statement.setString(2, artist.getSecondName());
            statement.setString(3, artist.getSurName());
            statement.setDate(4, artist.getDateOfBirthday());
            statement.setDate(5, artist.getDateOfStartCareer());
            statement.setDate(6, artist.getDateOfEndCareer());
            statement.setString(7, String.valueOf(artist.getSex()));
            statement.setString(8, artist.getCountry());
            statement.setString(9, artist.getCity());
            statement.setString(10, artist.getGenres());
            statement.setString(11, artist.getDescription());
            statement.setInt(12, artist.getLabelId());
            statement.setInt(13, artist.getMusicalGroupId());
            statement.setInt(14, artist.getId());
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in updateData" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Обновляем данные о музыкальной группе
    public void updateData(MusicalGroup musicalGroup) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UpdateMusicalGroupData);
            statement.setString(1, musicalGroup.getNameOfMusicalGroup());
            statement.setString(2, musicalGroup.getDescription());
            statement.setInt(3, musicalGroup.getLabelId());
            statement.setDate(4, musicalGroup.getDateOfStartCareer());
            statement.setDate(5, musicalGroup.getDateOfEndCareer());
            statement.setString(6, musicalGroup.getGenres());
            statement.setString(7, musicalGroup.getCountry());
            statement.setString(8, musicalGroup.getCity());
            statement.setInt(9, musicalGroup.getId());
        } catch (SQLException sqlex) {
            logger.info("SQLException in updateData" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Обновляем данные о лейбле
    public void updateData(Label label) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(UpdateLabelData);
            statement.setString(1, label.getNameOfLabel());
            statement.setString(2, label.getDescription());
            statement.setString(3, label.getFounder());
            statement.setDate(4, label.getFounded());
            statement.setString(5, label.getLocation());
            statement.setString(6, label.getCountryOfOrigin());
            statement.setInt(7, label.getId());
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in updateData" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Удаляем определённую музыкальную группу
    //Удаляем определённого музыканта/артиста
    //Удаляем определённый лейбл
    public void removeObjectFromSql(int objectId, int flag) {
        Connection connection = connectionInit();
        PreparedStatement statement = null;
        String deleteObject = null;
        try {
            switch (flag) {
                case 1 : deleteObject = DeletePerfomer;
                case 0 : deleteObject = DeleteMusicalGroup;
                case -1 : deleteObject = DeleteLabel;
            }
            statement = connection.prepareStatement(deleteObject);
            statement.setInt(1, objectId);
            statement.execute();
        } catch (SQLException sqlex) {
            logger.info("SQLException in removeObjectFromSql" + sqlex.getMessage());
        } finally {
            doCloseActiveStatement(statement);
            doCloseActiveConnection(connection);
        }
    }

    //Закрываем открытый ResultSet
    private void doCloseActiveResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in doCloseActiveConnection" + sqlex.getMessage());
        }
    }

    //Закрываем открытый statement
    private void doCloseActiveStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in doCloseActiveConnection" + sqlex.getMessage());
        }
    }

    //Закрываем открытый connection
    private void doCloseActiveConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException sqlex) {
            logger.info("SQLException in doCloseActiveConnection" + sqlex.getMessage());
        }
    }
}
