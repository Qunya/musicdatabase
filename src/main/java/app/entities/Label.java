package app.entities;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;

public class Label implements Comparable {
    //ID лейбла
    private int labelId;
    //Название лейбла
    private String nameOfLabel;
    //Описание лейбла
    private String description;
    //Владелец лейбла
    private String founder;
    //Дата основания
    private Date founded;
    //Местоположение лейбла
    private String location;
    //Страна происхождения лейбла
    private String countryOfOrigin;

    public Label() {
    }

    public Label(ResultSet resultSet) throws SQLException {
        int numbOfColumn = 1;
        setId(resultSet.getInt(numbOfColumn++));
        setNameOfLabel(resultSet.getString(numbOfColumn++));
        setDescription(resultSet.getString(numbOfColumn++));
        setFounder(resultSet.getString(numbOfColumn++));
        setFounded(resultSet.getDate(numbOfColumn++));
        setLocation(resultSet.getString(numbOfColumn++));
        setCountryOfOrigin(resultSet.getString(numbOfColumn++));
    }

    public int getId() {
        return labelId;
    }

    public void setId(int labelId) {
        this.labelId = labelId;
    }

    public String getNameOfLabel() {
        return nameOfLabel;
    }

    public void setNameOfLabel(String nameOfLabel) {
        this.nameOfLabel = nameOfLabel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public Date getFounded() {
        return founded;
    }

    public void setFounded(Date founded) {
        this.founded = founded;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCountryOfOrigin() {
        return countryOfOrigin;
    }

    public void setCountryOfOrigin(String countryOfOrigin) {
        this.countryOfOrigin = countryOfOrigin;
    }

    @Override
    public String toString() {
        return encodeString();
    }

    private String encodeString() {
        String string = "Название: " + nameOfLabel + "; ";
        if (stringChecker(founder)) {
            string += "Основатель: '" + founder + "'; ";
        }
        string += "Основан в: " + encodeDateFormat(founded) + "; ";
        if (stringChecker(location)) {
            string += "Место положение: " + location + "; ";
        }
        if (stringChecker(countryOfOrigin)){
            string += "Страна происхождения: " + countryOfOrigin + "; ";
        }
        if (stringChecker(description)){
            string += "Описание: " + description + ";";
        }
        return string;
    }

    private boolean stringChecker(String string) {
        if (string!=null && !string.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Label label = (Label) o;

        if (labelId != label.labelId) return false;
        return founder != null ? founder.equals(label.founder) : label.founder == null;
    }

    @Override
    public int hashCode() {
        int result = labelId;
        result = 31 * result + (founder != null ? founder.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        Label label = (Label) o;
        int result = Integer.compare(labelId, label.labelId);
        if (result != 0) {
            return result;
        }
        return result = founder.compareTo(label.founder);
    }

    //Преобразуем дату в нужный формат
    private String encodeDateFormat(Date date) {
        if (date != null) {
            return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
        } else {
            return "Нет";
        }
    }
}