package app.entities;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;

public class MusicalGroup implements Comparable {
    //ID музыкальной группы
    private int musicalGroupId;
    //Название музыкальной группы
    private String nameOfMusicalGroup;
    //Описание музыкальной группы
    private String description;
    //ID лейбла
    private int labelId;
    //Дата основания  музыкальной группы
    private Date dateOfStartCareer;
    //Дата распада музыкальной группы
    private Date dateOfEndCareer;
    //Жанр музыки музыкальной группы
    private String genres;
    //Страна музыкальной группы
    private String country;
    //Город музыкальной группы
    private String city;

    public MusicalGroup() {

    }

    public MusicalGroup(ResultSet resultSet) throws SQLException {
        int numbOfColumn = 1;
        setId(resultSet.getInt(numbOfColumn++));
        setNameOfMusicalGroup(resultSet.getString(numbOfColumn++));
        setDescription(resultSet.getString(numbOfColumn++));
        setLabelId(resultSet.getInt(numbOfColumn++));
        setDateOfStartCareer(resultSet.getDate(numbOfColumn++));
        setDateOfEndCareer(resultSet.getDate(numbOfColumn++));
        setGenres(resultSet.getString(numbOfColumn++));
        setCountry(resultSet.getString(numbOfColumn++));
        setCity(resultSet.getString(numbOfColumn++));
    }

    public int getId() {
        return musicalGroupId;
    }

    public void setId(int musicalGroupId) {
        this.musicalGroupId = musicalGroupId;
    }

    public String getNameOfMusicalGroup() {
        return nameOfMusicalGroup;
    }

    public void setNameOfMusicalGroup(String nameOfMusicalGroup) {
        this.nameOfMusicalGroup = nameOfMusicalGroup;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLabelId() {
        return labelId;
    }

    public void setLabelId(int labelId) {
        this.labelId = labelId;
    }

    public Date getDateOfStartCareer() {
        return dateOfStartCareer;
    }

    public void setDateOfStartCareer(Date dateOfStartCareer) {
        this.dateOfStartCareer = dateOfStartCareer;
    }

    public Date getDateOfEndCareer() {
        return dateOfEndCareer;
    }

    public void setDateOfEndCareer(Date dateOfEndCareer) {
        this.dateOfEndCareer = dateOfEndCareer;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return encodeString();
    }

    private String encodeString() {
        String string = "Название: " + nameOfMusicalGroup + "; ";
        if (labelId != 0){
            string += "ID лейбла: " + labelId + "; ";
        }
        string += "Дата основания: " + encodeDateFormat(dateOfStartCareer) + "; ";
        if (dateOfEndCareer != null) {
            string += "Дата завершения: " + encodeDateFormat(dateOfEndCareer) + "; ";
        }
        if (stringChecker(country)) {
            string += "Страна: " + country + "; ";
        }
        if (stringChecker(city)){
            string += "Город: " + city + "; ";
        }
        if (stringChecker(genres)){
            string += "Жанры: " + genres + "; ";
        }
        if (stringChecker(description)){
            string += "Описание: " + description + ";";
        }
        return string;
    }

    private boolean stringChecker(String string) {
        if (string!=null && !string.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MusicalGroup that = (MusicalGroup) o;

        if (musicalGroupId != that.musicalGroupId) return false;
        return nameOfMusicalGroup.equals(that.nameOfMusicalGroup);
    }

    @Override
    public int hashCode() {
        int result = musicalGroupId;
        result = 31 * result + nameOfMusicalGroup.hashCode();
        return result;
    }

    @Override
    public int compareTo(Object obj) {
        MusicalGroup musicalGroup = (MusicalGroup) obj;
        return Integer.compare(musicalGroupId, musicalGroup.musicalGroupId);
    }

    //Преобразуем дату в нужный формат
    private String encodeDateFormat(Date date) {
        if (date != null) {
            return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
        } else {
            return "Нет";
        }
    }
}
