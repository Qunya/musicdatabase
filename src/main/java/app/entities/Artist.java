package app.entities;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;

public class Artist implements Comparable {
    //ID музыканта/исполнителя/артиста
    private int perfomerId;
    //Имя музыканта/исполнителя/артиста
    private String firstName;
    //Второе имя музыканта/исполнителя/артиста
    private String secondName;
    //Фамилия музыканта/исполнителя/артиста
    private String surName;
    //Дата рождения музыканта/исполнителя/артиста
    private Date dateOfBirthday;
    //Дата начала карьеры музыканта/исполнителя/артиста
    private Date dateOfStartCareer;
    //Дата завершения карьеры музыканта/исполнителя/артиста
    private Date dateOfEndCareer;
    //Пол музыканта/исполнителя/артиста
    private char sex;
    //Страна музыканта/исполнителя/артиста
    private String country;
    //Город музыканта/исполнителя/артиста
    private String city;
    //Жанр музыки
    private String genres;
    //Описание музыканта
    private String description;
    //ID лейбла
    private int labelId;
    //ID музыкальной группы
    private int musicalGroupId;

    public Artist() {

    }

    public Artist(String firstName, String secondName, String surName, Date dateOfBirthday, Date dateOfStartCareer, Date dateOfEndCareer, String sex, String country, String city, String genres, String description) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.surName = surName;
        this.dateOfBirthday = dateOfBirthday;
        this.dateOfStartCareer = dateOfStartCareer;
        this.dateOfEndCareer = dateOfEndCareer;
        this.sex = sex.charAt(0);
        this.country = country;
        this.city = city;
        this.genres = genres;
        this.description = description;
        this.labelId = 0;
        this.musicalGroupId = 0;
    }

    public Artist(ResultSet resultSet) throws SQLException {
        int numberOfColumn = 1;
        setId(resultSet.getInt(numberOfColumn++));
        setFirstName(resultSet.getString(numberOfColumn++));
        setSecondName(resultSet.getString(numberOfColumn++));
        setSurName(resultSet.getString(numberOfColumn++));
        setDateOfBirthday(resultSet.getDate(numberOfColumn++));
        setDateOfStartCareer(resultSet.getDate(numberOfColumn++));
        setDateOfEndCareer(resultSet.getDate(numberOfColumn++));
        setSex(resultSet.getString(numberOfColumn++).charAt(0));
        setCountry(resultSet.getString(numberOfColumn++));
        setCity(resultSet.getString(numberOfColumn++));
        setGenres(resultSet.getString(numberOfColumn++));
        setDescription(resultSet.getString(numberOfColumn++));
    }

    public int getId() {
        return perfomerId;
    }

    public void setId(int perfomerId) {
        this.perfomerId = perfomerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfBirthday() {
        return dateOfBirthday;
    }

    public void setDateOfBirthday(Date dateOfBirthday) {
        this.dateOfBirthday = dateOfBirthday;
    }

    public Date getDateOfStartCareer() {
        return dateOfStartCareer;
    }

    public void setDateOfStartCareer(Date dateOfStartCareer) {
        this.dateOfStartCareer = dateOfStartCareer;
    }

    public Date getDateOfEndCareer() {
        return dateOfEndCareer;
    }

    public void setDateOfEndCareer(Date dateOfEndCareer) {
        this.dateOfEndCareer = dateOfEndCareer;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public int getLabelId() {
        return labelId;
    }

    public void setLabelId(int labelId) {
        this.labelId = labelId;
    }

    public int getMusicalGroupId() {
        return musicalGroupId;
    }

    public void setMusicalGroupId(int musicalGroupId) {
        this.musicalGroupId = musicalGroupId;
    }

    @Override
    public String toString() {
        return encodeString();
    }

    private String encodeString() {
        String string = "Имя: " + firstName + " " + surName + "; ";
        if (stringChecker(secondName)) {
            string += "Второе имя: '" + secondName + "'; ";
        }
        string += "Дата рождения: " + encodeDateFormat(dateOfBirthday) + "; " +
                "Начало карьеры: " + encodeDateFormat(dateOfStartCareer) + "; ";
        if (dateOfEndCareer != null) {
            string += "Завершение карьеры: " + encodeDateFormat(dateOfEndCareer) + "; ";
        }
        string = string + "Пол: " + sex + "; ";
        if (stringChecker(country)) {
            string += "Страна: " + country + "; ";
        }
        if (stringChecker(city)){
            string += "Город: " + city + "; ";
        }
        if (stringChecker(genres)){
            string += "Жанры: " + genres + "; ";
        }
        if (stringChecker(description)){
            string += "Описание: " + description + ";";
        }
        if (labelId != 0){
            string += "ID лейбла: " + labelId + "; ";
        }
        if (musicalGroupId != 0){
            string += "ID музыкальной группы" + musicalGroupId + "; ";
        }
        return string;
    }

    private boolean stringChecker(String string) {
            if (string!=null && !string.isEmpty()) {
                return true;
            } else {
                return false;
            }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Artist artist = (Artist) o;

        return perfomerId == artist.perfomerId;
    }

    @Override
    public int hashCode() {
        return perfomerId;
    }

    @Override
    public int compareTo(Object o) {
        Artist artist = (Artist) o;
        int result = Integer.compare(perfomerId, artist.perfomerId);
        return result;
    }

    //Преобразуем дату в нужный формат
    private String encodeDateFormat(Date date) {
        if (date != null) {
            return DateFormat.getDateInstance(DateFormat.DEFAULT).format(date);
        } else {
            return "Нет";
        }
    }
}
