<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: ReaGame
  Date: 01.04.2018
  Time: 0:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="el" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список пользователей</title>
</head>
<body>
<div><h4>Список артистов</h4>
    <ul>
    <el:forEach items="${artists}" var="list">
        <li>${list}</li>
    </el:forEach>
    </ul>
</div>
<div><h4>Список музыкантов</h4>
    <ul>
        <el:forEach items="${groups}" var="list">
            <li>${list}</li>
        </el:forEach>
    </ul>
</div>
<div><h4>Список лейблов</h4>
    <ul>
        <el:forEach items="${labels}" var="list">
            <li>${list}</li>
        </el:forEach>
    </ul>
</div>
</body>
</html>
