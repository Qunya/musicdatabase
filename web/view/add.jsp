<%--
  Created by IntelliJ IDEA.
  User: ReaGame
  Date: 01.04.2018
  Time: 0:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Регистрация</title>
</head>
<body>
<div><h3>Регистрация</h3></div>
<form method="post">
    <tr>
        <td>Выбор кем хотите быть</td>
        <td>
            <select name="possible-result">
                <option value="Label">Лейбл</option>
                <option value="Artist">Артист</option>
                <option value="MusicalGroup">Музыкальная группа</option>
            </select>
        </td>
        <br/>
    </tr>
    
    <label>Имя:
        <input type="text" name="firstName"><br/>
    </label>

    <label>Фамилия:
        <input type="text" name="surName"><br/>
    </label>

    <label>Никнейм:
        <input type="text" name="secondName"><br/>
    </label>

    <label>Дата рождения:
        <input type="date" name="dateOfBirthday"><br/>
    </label>

    <label>Дата начала карьеры:
        <input type="date" name="dateOfStartCareer"><br/>
    </label>

    <label>Дата завершения карьеры:
        <input type="date" name="dateOfEndCareer"><br/>
    </label>

    <label>Пол:
        <select name="sex">
            <option value="M">Мужской</option>
            <option value="F">Женский</option>
        </select>
        <br/>
    </label>

    <label>Страна:
        <input type="text" name="country"><br/>
    </label>

    <label>Город:
        <input type="text" name="city"><br/>
    </label>

    <label>Жанры:
        <input type="text" name="genres"><br/>
    </label>

    <label>Описание:
        <input type="text" name="description"><br/>
    </label>
    <button type="submit">Submit</button>
</form>
</body>
</html>
